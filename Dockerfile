FROM maven:3.8.5-openjdk-17

WORKDIR /workspace

COPY target/chatapp-0.0.1-SNAPSHOT.jar /workspace/chatapp.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/workspace/chatapp.jar"]