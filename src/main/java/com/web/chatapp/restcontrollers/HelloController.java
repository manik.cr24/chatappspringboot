package com.web.chatapp.restcontrollers;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/add")
    public String add() {
        return "ADDING";
    }

    @GetMapping("/delete")
    public String delete() {
        return "DELETING";
    }
}
